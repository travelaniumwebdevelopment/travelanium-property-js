const PACKAGE = require('./package.json');
const {task, src, dest, watch, parallel, series} = require('gulp');
const webpack = require('webpack-stream');
const browser = require('browser-sync').create();
const plumber = require('gulp-plumber');
const TerserPlugin = require('terser-webpack-plugin');

const ENV = process.env.NODE_ENV || 'production';

const FILE = {
  name: 'travelanium-property.js',
  version: PACKAGE.version,
}

const reload = done => {
  browser.reload();
  done();
}

task('bundle', done => {
  let outputPath = ENV === 'production' ? FILE.version.concat('/', FILE.name) : FILE.name;

  src([
    './src/index.js'
  ])
    .pipe(plumber())
    .pipe(webpack({
      mode: ENV,
      devtool: 'production' === ENV ? false : 'source-map',
      output: {
        filename: outputPath,
      },
      module: {
        rules: [
          {
            test: /.js$/,
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env'],
            },
          }
        ]
      },
      optimization: {
        minimizer: [
          new TerserPlugin({
            extractComments: false,
          })
        ]
      },
    }))
    .pipe(dest( () => {
      if ('production' === ENV)
        return './build';
      return './dist';
    } ))

  done();
});

task('watch', done => {
  watch(['./src/**/*.js'], series('bundle', reload));
  watch(['./index.html'], series(reload));

  done();
});

task('server', done => {
  browser.init({
    open: false,
    server: true,
    cors: true,
    ignore: [
      'node_modules'
    ]
  })
  
  done();
});

task('build', parallel('bundle'));
task('serve', parallel('build', 'watch', 'server'));