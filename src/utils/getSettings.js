var PARAMS = {
  apiKey: '',
  apiVersion: null,
  lang: '',
  currency: '',
  propertyId: '',
  callback: undefined,
}

let s = Array.prototype.find.call(document.scripts, script => script.src.indexOf('travelanium-property.js') > -1);
let params = s.src.split('?')[1];
if(params) {
  let query = params.split('&');
  query.forEach(p => {
    let param = p.split('=');
    PARAMS[param[0]] = param[1];
  });
}

export default PARAMS;