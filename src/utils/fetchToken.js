import Cookies from "js-cookie";

var BASE_AUTH_URL = 'https://graph.travelanium.net/api/auth';

export function fetchToken( API_KEY ) {
  return new Promise((resolve, reject) => {
    let TOKEN = Cookies.get('_tlg');

    if ( !TOKEN ) {
      var getToken = fetch( BASE_AUTH_URL, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'X-API-KEY': API_KEY,
        }
      } );
  
      var response = getToken.then( res => res.text() );
  
      response.then( res => {
        let TOKEN = res;
        if ( !res.errors || res !== '' ) {
          resolve( `Bearer ${TOKEN}` );
          Cookies.set('_tlg', TOKEN, {
            expires: 0.3,
          });
        } else {
          reject( res.errors );
        }
      } );
    } else {
      resolve( `Bearer ${TOKEN}` );
    }
  });
}

export function getToken() {
  let TOKEN = Cookies.get('_tlg');
  return `Bearer ${TOKEN}`;
}

export default {
  fetchToken,
  getToken
}