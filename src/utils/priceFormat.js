import currency from 'currency.js';

/**
 * Convert number to string price formatted
 *
 * @param {string|number} number 
 * @param {object} options
 * @returns NumberFormatted
 *
 * @view https://currency.js.org/
 */
export function priceFormat( number, options ) {
  return currency(number, {
    precision: 0,
    pattern: '#',
    ...options,
  }).format();
}
