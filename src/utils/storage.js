/**
 * Get stored data
 * 
 * @param {string} key 
 * @param {number} exp 
 * @returns CacheData
 */
 export function get(key) {
  var data = localStorage.getItem(key);

  if (!data) {
    return null;
  } else if ( JSON.parse(data).ts < new Date().getTime() ) {
    return null;
  } else {
    return JSON.parse(data).val;
  }
}

/**
 * Store data in local storage
 * 
 * @param {string} key 
 * @param {any} val 
 * @param {number} timestamp 
 */
export function set(key, val, expireInMinuts) {
  let exp = expireInMinuts * 60000;
  let timestamp = new Date().getTime() + exp;
  localStorage.setItem( key, JSON.stringify({
    val: val,
    ts: timestamp
  }) );
}

export default {
  get,
  set
};