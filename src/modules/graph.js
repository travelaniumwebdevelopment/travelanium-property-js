import PARAMS from '../utils/getSettings';
import storage from '../utils/storage';
import Cookies from 'js-cookie';
import {getToken} from '../utils/fetchToken'; 
import md5 from 'md5';

const PROPERTY_ENDPOINT = 'https://graph.travelanium.net/api/v1/property';

/**
 * Get rateplans graph query
 * 
 * @param {number} apiVersion 
 * @param {string} propertyId 
 * @param {string} currency 
 * @param {string} lang 
 * @returns RatePlansGraphQuery
 */
function getRatePlansGraph( apiVersion, propertyId, currency, lang ) {
  var query = {
    "query" : `query GetRatePlans {
      rateplans( apiVersion: ${apiVersion} propertyId: "${propertyId}" currency: "${currency}" lang: "${lang}" ) {
        code
        name
        productType
        infoURL
        bookingURL
        bookingPeriods {
          dayOfWeek {
            day
            time {
              from
              to
            }
          }
          from
          to
        }
        images {
          alt
          size {
            thumbnail {
              src
              width
              height
            }
            medium {
              src
              width
              height
            }
            large {
              src
              width
              height
            }
            full {
              src
              width
              height
            }
          }
        }
        startingRate {
          rate
          finalRate
          fullRate
          finalFullRate
        }
        startingRatePerNight {
          rate
          finalRate
          fullRate
          finalFullRate
        }
        startingRatePerNightPerPerson {
          rate
          finalRate
          fullRate
          finalFullRate
        }
      }
    }`
  }

  return JSON.stringify(query);
}

/**
 * Get specific rate plan bt code
 * 
 * @param {number} apiVersion 
 * @param {string} propertyId 
 * @param {string} currency 
 * @param {string} lang 
 * @param {string} ratePlanCode 
 * @returns RatePlanGraphQuery
 */
function getRatePlanGraph( apiVersion, propertyId, currency, lang, ratePlanCode ) {
  var query = {
    "query" : `query GetRatePlan {
      rateplan( apiVersion: ${apiVersion} propertyId: "${propertyId}" currency: "${currency}" lang: "${lang}" ratePlanCode: "${ratePlanCode}" ) {
        code
        name
        productType
        infoURL
        bookingURL
        bookingPeriods {
          dayOfWeek {
            day
            time {
              from
              to
            }
          }
          from
          to
        }
        images {
          alt
          size {
            thumbnail {
              src
              width
              height
            }
            medium {
              src
              width
              height
            }
            large {
              src
              width
              height
            }
            full {
              src
              width
              height
            }
          }
        }
        startingRate {
          rate
          finalRate
          fullRate
          finalFullRate
        }
        startingRatePerNight {
          rate
          finalRate
          fullRate
          finalFullRate
        }
        startingRatePerNightPerPerson {
          rate
          finalRate
          fullRate
          finalFullRate
        }
      }
    }`,
    "operationName": "GetRatePlan"
  }

  return JSON.stringify(query);
}

/**
 * Get room type graph query
 *
 * @param {number} apiVersion 
 * @param {string} propertyId 
 * @param {string} currency 
 * @param {string} lang 
 * @returns RoomTypesGraphQuery
 */
function getRoomTypesGraph( apiVersion, propertyId, currency, lang ) {
  var query = {
    "query": `query GetRoomtypes {
      roomtypes ( apiVersion: ${apiVersion} propertyId: "${propertyId}" currency: "${currency}" lang: "${lang}" ) {
        code
        name
        bookingURL
        images {
          alt
          size {
            thumbnail {
              src
              width
              height
            }
            medium {
              src
              width
              height
            }
            large {
              src
              width
              height
            }
            full {
              src
              width
              height
            }
          }
        }
        startingRate {
          rate
          finalRate
          fullRate
          finalFullRate
        }
        startingRatePerNight {
          rate
          finalRate
          fullRate
          finalFullRate
        }
        startingRatePerNightPerPerson {
          rate
          finalRate
          fullRate
          finalFullRate
        }
      }
    }`
  }

  return JSON.stringify(query);
}

/**
 * Get specific room type graph query
 * 
 * @param {number} apiVersion 
 * @param {string} propertyId 
 * @param {string} currency 
 * @param {string} lang 
 * @param {string} roomTypeCode 
 * @returns RoomTypeGraphQuery
 */
function getRoomTypeGraph( apiVersion, propertyId, currency, lang, roomTypeCode ) {
  var query = {
    "query": `query GetRoomtype {
      roomtype ( apiVersion: ${apiVersion} propertyId: "${propertyId}" currency: "${currency}" lang: "${lang}" roomtypeCode: "${roomTypeCode}" ) {
        code
        name
        bookingURL
        images {
          alt
          size {
            thumbnail {
              src
              width
              height
            }
            medium {
              src
              width
              height
            }
            large {
              src
              width
              height
            }
            full {
              src
              width
              height
            }
          }
        }
        startingRate {
          rate
          finalRate
          fullRate
          finalFullRate
        }
        startingRatePerNight {
          rate
          finalRate
          fullRate
          finalFullRate
        }
        startingRatePerNightPerPerson {
          rate
          finalRate
          fullRate
          finalFullRate
        }
      }
    }`
  }

  return JSON.stringify(query);
}

/**
 * Get rate plans data
 *
 * @param {object} query 
 * @returns RateplansData
 */
export function getRatePlans( query ) {
  var configs = {
    apiVersion: PARAMS.apiVersion,
    propertyId: PARAMS.propertyId,
    currency: PARAMS.currency,
    lang: PARAMS.lang,
  }

  Object.assign(configs, query);

  var token = getToken();
  var hash = md5('rateplans' + JSON.stringify(configs));

  return new Promise((resolve, reject) => {
    let data = storage.get(hash);
    
    if (!data) {
      var fetchRatePlan = fetch( PROPERTY_ENDPOINT, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': token,
        },
        body: getRatePlansGraph(configs.apiVersion, configs.propertyId, configs.currency, configs.lang),
      } );
      
      var response = fetchRatePlan.then(res => {
        if (res.ok) {
          return res.json();
        }
      });
      
      response.then(res => {
        if (!res.errors) {
          storage.set(hash, res, 15);
          resolve(res);
        } else {
          reject( res.errors );
        }
      })
    } else {
      resolve(data);
    }
  });
};

/**
 * Get specific rate plan data by code
 *
 * @param {string} code 
 * @returns RatePlanData
 */
export function getRatePlan( code, query ) {
  var configs = {
    apiVersion: PARAMS['apiVersion'],
    propertyId: PARAMS['propertyId'],
    currency: PARAMS['currency'],
    lang: PARAMS['lang'],
  }

  Object.assign(configs, query);
  
  return new Promise((resolve, reject) => {
    let request = fetch( PROPERTY_ENDPOINT, {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        "Authorization": Cookies.get('_tlg'),
      },
      body: getRatePlanGraph(configs.apiVersion, configs.propertyId, configs.currency, configs.lang, code),
    } );

    let response = request.then( res => res.json() );

    response.then( result => {
      if (result.errors) {
        reject( result.errors );
      } else {
        resolve( result.data.rateplan );
      }
    } );
  });
}

/**
 * Get room types data
 *
 * @param {object} query 
 * @returns RoomTypesData
 */
export function getRoomTypes( query ) {
  var configs = {
    apiVersion: PARAMS.apiVersion,
    propertyId: PARAMS.propertyId,
    currency: PARAMS.currency,
    lang: PARAMS.lang,
  }

  Object.assign(configs, query);

  var token = getToken();
  var hash = md5('roomtypes' + JSON.stringify(configs));
  
  return new Promise((resolve, reject) => {
    let data = storage.get(hash);
    
    if (!data) {
      var fetchRoomTypes = fetch( PROPERTY_ENDPOINT, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': token,
        },
        body: getRoomTypesGraph(configs.apiVersion, configs.propertyId, configs.currency, configs.lang),
      } );
      
      var response = fetchRoomTypes.then(res => {
        if (res.ok) {
          return res.json();
        }
      });
      
      response.then(res => {
        if (!res.errors) {
          storage.set(hash, res, 15);
          resolve(res);
        } else {
          reject( res.errors );
        }
      })
    } else {
      resolve(data);
    }
  });
}

/**
 * Get specific room type data by code
 *
 * @param {string} code 
 * @param {object} query 
 * @returns RoomTypeData
 */
export function getRoomType( code, query ) {
  var configs = {
    apiVersion: PARAMS['apiVersion'],
    propertyId: PARAMS['propertyId'],
    lang: PARAMS['lang'],
    currency: PARAMS['currency'],
  }

  Object.assign(configs, query);
  
  return new Promise((resolve, reject) => {
    let request = fetch( PROPERTY_ENDPOINT, {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        "Authorization": Cookies.get('_tlg'),
      },
      body: getRoomTypeGraph(configs.apiVersion, configs.propertyId, configs.currency, configs.lang, code),
    } );

    let response = request.then( res => res.json() );

    response.then( result => {
      if (result.errors) {
        reject( result.errors );
      } else {
        resolve( result.data.roomtype );
      }
    } );
  });
}
