import PARAMS from './utils/getSettings';
import {fetchToken} from './utils/fetchToken';
import {getRatePlan, getRatePlans, getRoomType, getRoomTypes} from './modules/graph';
import {priceFormat} from './utils/priceFormat';

var graph = {
  getRatePlan,
  getRatePlans,
  getRoomType,
  getRoomTypes
}

var utilities = {
  priceFormat
}

var methods = {
  graph: graph,
  utils: utilities
}

if ('undefined' === typeof window.tl) {
  window.TL = {}
}

if ('undefined' === typeof window.TL.utils) {
  window.TL.utils = {};
}

function addLoadingClass() {
  let rateTags = document.querySelectorAll('[data-rate-id]');
  let roomTags = document.querySelectorAll('[data-room-id]');
  Array.prototype.forEach.call(rateTags, tag => tag.classList.add('loading'));
  Array.prototype.forEach.call(roomTags, tag => tag.classList.add('loading'));
}

if ('loading' === document.readyState) {
  document.addEventListener('DOMContentLoaded', addLoadingClass())
} else {
  addLoadingClass();
}

fetchToken( PARAMS.apiKey ).then(res => {
  window.TL['graph'] = graph;
  window.TL.utils['priceFormat'] = priceFormat;

  Promise.all([getRatePlans(), getRoomTypes()]).then(res => {
    if ( 'loading' === document.readyState ) {
      document.addEventListener('DOMContentLoaded', () => {
        initRateplanTag();
        initRoomtypeTag();
        initCallback(methods);
      });
    } else {
      initRateplanTag();
      initRoomtypeTag();
      initCallback(methods);
    }
  })
});

 function initRateplanTag() {
   let tags = Array.prototype.slice.call(document.querySelectorAll('[data-rate-id]'));

   getRatePlans().then(res => {
    let data = res.data.rateplans;
    tags.forEach( el => {
      var id = el.dataset.rateId;
      var field = el.dataset.field;
      var subfield = el.dataset.subfield;
      var prefix = el.dataset.prefix ?? '';
      var suffix = el.dataset.suffix ?? '';
      var item = data.find(item => item.code === id);

      if (item) {
        el.innerText = prefix + priceFormat( item[field][subfield] ) + suffix;
      } else {
        el.innerText = 'N/A';
      }

      el.classList.remove('loading');
      el.classList.add('loaded');
    })
  });
}

function initRoomtypeTag() {
  var tags = Array.prototype.slice.call(document.querySelectorAll('[data-room-id]'));

  getRoomTypes().then(res => {
    let data = res.data.roomtypes;
    tags.forEach( el => {
      var id = el.dataset.roomId;
      var field = el.dataset.field;
      var subfield = el.dataset.subfield;
      var prefix = el.dataset.prefix ?? '';
      var suffix = el.dataset.suffix ?? '';
      var item = data.find( item => item.code === id );

      if (item) {
        el.innerText = prefix + priceFormat( item[field][subfield] ) + suffix;
      } else {
        el.innerText = 'N/A';
      }

      el.classList.remove('loading');
      el.classList.add('loaded');
    })
  });
}

function initCallback() {
  let callback = PARAMS.callback;
  if ('function' === typeof window[callback]) {
    window[callback](methods);
  }
}

export default methods;