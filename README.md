# Travelanium Property JS

สคริปต์ที่ช่วยในการเชื่อมต่อและดึงข้อมูลต่าง ๆ ปัจจุบันสามารถใช้ดึงราคาเรทและราคาห้องพักได้

## Table of Contents

- [Travelanium Property JS](#travelanium-property-js)
  - [Table of Contents](#table-of-contents)
  - [การติดตั้ง](#การติดตั้ง)
  - [การดึงราคา Rate](#การดึงราคา-rate)
  - [การดึงราคา Room](#การดึงราคา-room)
  - [Rate ID และ Room ID](#rate-id-และ-room-id)
  - [Field และ Subfield](#field-และ-subfield)
    - [Field คือ ประเภทของราคา](#field-คือ-ประเภทของราคา)
    - [Subfield คือ ประเภทย่อยของราคา](#subfield-คือ-ประเภทย่อยของราคา)

---

## การติดตั้ง

ทำการติดตั้งไฟล์สคริปต์ไว้ในส่วนของ `<head>` หรือในส่วนของ `<body>`

```html
<html>
  <head>
    ...
    <script async defer src="https://hotelresources.travelanium.net/propertysite/scripts/v1/travelanium-property.js?apiKey=[your_api_key]&propertyId=[your_property_id]&apiVersion=1&currency=THB&lang=en"></script>
  </head>
  <body>
    ...
  </body>
</html>
```

| Parameter     | Description |
| :---          | :--- |
| apiKey        | **required** - คีย์ที่ได้รับอนุญาตให้สามารถเรียกของมูลจากระบบได้  |
| propertyId    | **required** - ID ของ property ที่ต้องการดึงข้อมูลมาแสดง |
| apiVersion    | **required** - เวอร์ชันของ api, <br /> **default**: 1 |
| currency      | **required** - สกุลเงินของ rate ที่ต้องการ <br />**possible value**: (Currency code: [ISO 4217](https://th.wikipedia.org/wiki/ISO_4217)) CNY / EUR / ILS / THB / USD / VND / AUD / GBP / MMK / KHR / CAD / DKK / EGP / HKD / INR / IDR / JPY / KZT / KWD / LAK / MYR / MVR / TWD / NZD / NOK / PHP / RUB / SGD / ZAR / KRW / SEK / CHF / TRY / AED |
| lang          | **required** - ภาษาของ rate ที่ต้องการ <br /> **possible value** - en / de / ru / th / zh / zh_TW / ja / ko / vi / fr |

เมื่อติดตั้งสคริปต์พร้อมกำหนดพาราเมเตตอร์ที่จำเป็นแล้ว ในส่วนของการแสดงผลราคาให้ทำตามวิธีการต่อไปนี้

## การดึงราคา Rate

ให้สร้าง `<span>` ที่มี attributes ดังตัวอย่างโค้ดต่อไปนี้ เพื่อเป็นตัวกำหนดว่าจะดึงราคาใดมาแสดงใน `<span>` อย่างไรก็ตาม `<span>` สามารถเปลี่ยนเป็นแท็กอื่นได้ตามความเหมาะสมอย่างไม่มีข้อจำกัด เช่น `<div>` หรือ `<del>`

**code:**
```html
<p>
  price: <span data-rate-id="PRO_1234" data-field="startingRate" data-subfield="fullRate" data-prefix="฿"></span>
</p>
```

**result:**

![Alt Display example](/img/ex_rateplan.png)

| Attributes      | Description |
| :---            | :--- |
| data-rate-id    | **required** - ID ของ rate ราคานั้น |
| [data-field](#markdown-header-field)      | **required** - เลือก field ที่ต้องการ มีด้วยกัน 3 ประเภท ได้แก่ startingRate, startingRatePerNight, startingRatePerNightPerPerson |
| [data-subfield](#markdown-header-subfield)   | **required** - เลือก subfield ของราคา ได้แก่ rate, finalRate, fullRate, finalFullRate |
| data-prefix     | **optional** - กำหนดข้อความที่จะอยู่ก่อนหน้า |
| data-suffix     | **optional** - กำหนดข้อความที่จะอยู่ต่อท้าย |

## การดึงราคา Room

เช่นเดียวกับการดึงราคา Rate, ต่างกันเพียง Attribute บางตัวเท่านั้น โดยให้สร้าง `<span>` ที่มี attributes ดังตัวอย่างโค้ดต่อไปนี้ เพื่อเป็นตัวกำหนดว่าจะดึงราคาใดมาแสดงใน `<span>`

อย่างไรก็ตาม `<span>` สามารถเปลี่ยนเป็นแท็กอื่นได้ตามความเหมาะสมอย่างไม่มีข้อจำกัด เช่น `<div>` หรือ `<del>`

**code:**

```html
<p>
  price: <span data-room-id="RM_1234" data-field="startingRate" data-subfield="fullRate" data-prefix="฿"></span>
</p>
```

**result:**

![Alt Display example](/img/ex_roomtype.png)

| Attributes      | Description |
| :---            | :--- |
| data-room-id    | **required** - ID ของ room นั้น |
| [data-field](#markdown-header-field)      | **required** - เลือก field ที่ต้องการ มีด้วยกัน 3 ประเภท ได้แก่ startingRate, startingRatePerNight, startingRatePerNightPerPerson |
| [data-subfield](#markdown-header-subfield)   | **required** - เลือก subfield ของราคา ได้แก่ rate, finalRate, fullRate, finalFullRate |
| data-prefix     | **optional** - กำหนดข้อความที่จะอยู่ก่อนหน้า |
| data-suffix     | **optional** - กำหนดข้อความที่จะอยู่ต่อท้าย |

---

## Rate ID และ Room ID

สำหรับข้อมูล ID ดังกล่าวสามารถหาได้จากภายในระบบของ Travelanium ในส่วนของเมนู Content ID Mapping

บริเวณแถบเมนูด้านซ้าย ในส่วนของ Setup Menu ให้คลิกที่เมนู Content ID Mapping

![Alt Comtent ID Mapping menu](/img/ex_menu.png)

เมื่อเข้ามาใช้หน้า Content ID Mapping แล้ว ให้คลิกแท็บ Rate เพื่อดูข้อมูลเรตราคาต่าง ๆ ที่สร้างไว้ในระบบ หรือ Room เพื่อดูเรตราคาห้อง

![Alt Content ID Mapping page](/img/ex_code_page.png)

## Field และ Subfield

### Field คือ ประเภทของราคา

| Field                           | Description |
| :---                            | :--- |
| startingRate                    | ราคาเริ่มต้น |
| startingRatePerNight            | ราคาเริ่มต้น ต่อหนึ่งคืน |
| startingRatePerNightPerPerson   | ราคาเริ่มต้น ต่อหนึ่งคน ต่อหนึ่งคืน |

### Subfield คือ ประเภทย่อยของราคา

| Subfield        | Description |
| :---            | :--- |
| rate            | ราคาที่ลดแล้วแบบ**ไม่รวม** VAT และ Service Charge |
| fullRate        | ราคาเต็มแบบ**ไม่รวม** VAT และ Service Charge |
| finalRate       | ราคาที่ลดแล้วแบบ**รวม** VAT และ Service Charge |
| finalFullRate   | ราคาเต็มแบบ**รวม** VAT และ Service Charge |